//Handles response object shared between middleware and sends response to client
package response

import (
	"bytes"
	"errors"
	"gitlab.com/jhfylling/gohttp2/gohttp2"
	"gitlab.com/jhfylling/gohttp2/h2connection"
	"gitlab.com/jhfylling/gohttp2/settings"
	"golang.org/x/net/http2/hpack"
	"net"
	"strconv"
)

//Response struct meant to be passed from middleware to middleware and then send a response to client
type Response struct{
	Header map[string]string
	con net.Conn
	encoder *hpack.Encoder
	headerBuffer *bytes.Buffer
	clientSettings *settings.Settings
	streamIdentifier [4]byte
	h2c *h2connection.H2Connection
}


//Creates a new response object with status 200, and version
//Also contains con and encoder
func NewResponse(con net.Conn, encoder *hpack.Encoder, headerBuffer *bytes.Buffer, clientSettings *settings.Settings, h2c *h2connection.H2Connection) Response{
	return Response{
		Header: map[string]string{
			":status": "200",
			"version": "HTTP/2.0",
		},
		con: con,
		encoder: encoder,
		headerBuffer: headerBuffer,
		clientSettings: clientSettings,
		h2c: h2c,
	}
}

//Used by endpoints to send data with contentType as http's content type
//Constructs necessary headers and frames
func (res *Response) Send(data []byte, contentType string) error{
	//Convert response header to []hpack.HeaderField
	var responseHeader []hpack.HeaderField
	for key, val := range res.Header{
		responseHeader = append(responseHeader, hpack.HeaderField{Name: key, Value: val})
	}

	//Set content-length and content-type
	responseHeader = append(responseHeader, hpack.HeaderField{Name: "content-length", Value: strconv.Itoa(len(data))})
	responseHeader = append(responseHeader, hpack.HeaderField{Name: "content-type", Value: contentType})

	//Send header
	err := gohttp2.SendHeader(responseHeader, res.streamIdentifier, res.h2c)
	if err != nil{
		return errors.New("Error sending header: " + err.Error())
	}

	//Send data
	err = gohttp2.SendData(data, res.streamIdentifier, res.clientSettings, res.h2c)
	if err != nil{
		return errors.New("Error sending header: " + err.Error())
	}


	return nil
}

//Set response header fields
func (res *Response) Set(key string, value string){
	res.Header[key] = value
}

//Set response object's stream identifier
func (res *Response) SetStreamIdentifier(streamIdentifier [4]byte){
	res.streamIdentifier = streamIdentifier
}

