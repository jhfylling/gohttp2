//This file binds together the library so the user can import gohttp2 only
package goh2

import (
	"gitlab.com/jhfylling/gohttp2/app"
	"gitlab.com/jhfylling/gohttp2/header"
	"gitlab.com/jhfylling/gohttp2/middleware"
	"gitlab.com/jhfylling/gohttp2/response"
)

//initiate server settings and add middleware to app
var NewApp = app.NewApp

//use Listen with specified address:port and app
var Listen = middleware.Listen

//request object shared by all middleware
type Request = header.Request

//response object shared by all middleware
type Response = response.Response
