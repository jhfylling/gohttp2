//gohttp2 package is used to serve static files and to send certain http responses
package gohttp2

import (
	"errors"
	"fmt"
	"gitlab.com/jhfylling/gohttp2/frame"
	"gitlab.com/jhfylling/gohttp2/h2connection"
	"gitlab.com/jhfylling/gohttp2/header"
	"gitlab.com/jhfylling/gohttp2/settings"
	"golang.org/x/net/http2/hpack"
	"io/ioutil"
	"net"
	"strconv"
	"strings"
)

//Used for reading request headers and responding with response header and data
//Sends encoded header and currently uncompressed data to con
func ServeStaticRequest(headerFrame frame.Http2frame, publicFolder string, h2c *h2connection.H2Connection){
	//Create Http2header object from the headerFrame (decoding and marshalling)
	requestHeader := header.NewHttp2Header(headerFrame, h2c)

	var responseHeader []hpack.HeaderField //Unencoded HeaderFields making up the response header

	//Check if requested path is safe. Send error message if .. is in requested path
	if strings.Contains(requestHeader.Path, ".."){
		err := SendError(404, &requestHeader, h2c) //Just use 404 for this to obscure what we are actually doing
		if err != nil{
			fmt.Println("Error sending HTTP error: ", err.Error())
		}
		return
	}

	//Read requested file from filesystem
	//file, err := ioutil.ReadFile("." + requestHeader.Path) //send requested file to client
	file, err := ioutil.ReadFile(publicFolder + requestHeader.Path) //send requested file to client
	if err != nil{
		fmt.Println(err.Error())
		err := SendError(404, &requestHeader, h2c)
		if err != nil{
			fmt.Println("Error sending HTTP error: ", err.Error())
		}
		return
	}

	splitFilePath := strings.Split(requestHeader.Path, ".")
	fileExtension := splitFilePath[len(splitFilePath)-1]
	contentType := "application/octet-stream" //Just send the binary data if we can't find a content type
	switch fileExtension {
	case "html":
		contentType = "text/html"
	case "css":
		contentType = "text/css"
	case "jpg":
		contentType = "image/jpg"
	case "svg":
		contentType = "image/svg"
	case "ico":
		contentType = "image/icon"
	}

	//Construct response header
	responseHeader = []hpack.HeaderField{
		{Name: ":status", Value: "200"},
		{Name: "version", Value: "HTTP/2.0"},
		{Name: "content-length", Value: strconv.Itoa(len(file))},
		{Name: "content-type", Value: contentType},
	}

	streamIdentifier := requestHeader.StreamIdentifier //Use the stream identifier from the request for the response

	//Send response header
	err = SendHeader(responseHeader, streamIdentifier, h2c)
	if err != nil{
		fmt.Println(err.Error())
	}

	fmt.Println("Sent header")


	//Create and send data frame. Fragment data if necessary
	err = SendData(file, streamIdentifier, &h2c.ClientSettings, h2c)
	if err != nil{
		fmt.Println(err.Error())
	}

	h2c.HeaderBuffer.Reset() //reset buffer, since it's created for the connection

	fmt.Println("Sent header and data")
}

//Creates data frames and fragments data into several frames if necessary
func SendData(payload []byte, streamIdentifier [4]byte, clientSettings *settings.Settings, h2c *h2connection.H2Connection) error{
	payloadLength := uint32(len(payload))
	if payloadLength > clientSettings.SETTINGS_MAX_FRAME_SIZE{
		//Send payload as several fragments
		increment := clientSettings.SETTINGS_MAX_FRAME_SIZE
		fragmentStart := uint32(0)
		fragmentEnd := increment
		for ;fragmentStart < payloadLength - increment; fragmentEnd += increment{
			//Create and send data frame
			dataFrame := frame.NewDataFrame(payload[fragmentStart:fragmentEnd], streamIdentifier, byte(0)) //flag 0x1 means last frame on stream from sender
			//_, err := con.Write(dataFrame)
			err := h2c.Write(dataFrame)
			if err != nil{
				return errors.New("Error writing dataFrame fragment: " + err.Error())
			}

			fragmentStart += increment
		}

		remainingByteCount := payloadLength - fragmentStart
		if remainingByteCount > 0{
			//Create and send data frame
			dataFrame := frame.NewDataFrame(payload[fragmentStart:fragmentStart + remainingByteCount], streamIdentifier, byte(1)) //flag 0x1 means last frame on stream from sender
			//_, err := con.Write(dataFrame)
			err := h2c.Write(dataFrame)
			if err != nil{
				return errors.New("Error writing last dataFrame fragment: " + err.Error())
			}
		}
	}else{
		//Send all data in one frame
		//Create and send data frame
		dataFrame := frame.NewDataFrame(payload, streamIdentifier, byte(1)) //flag 0x1 means last frame on stream from sender

		//_, err := con.Write(dataFrame)
		err := h2c.Write(dataFrame)
		if err != nil{
			return errors.New("Error writing dataFrame: " + err.Error())
		}
	}

	return nil
}

//TODO: Create header fragmentation functionality, using continuation frames.

//Encodes header, wraps it in a header frame and sends it on con
//responseHeader contains hpack format header fields (not encoded)
//hpackEncoder reference to hpack encoder object with dynamic table
//streamIdentifier
//headerBuffer the buffer used by the encoder
//error if con.Write returns error or nil if no error
func SendHeader(responseHeader []hpack.HeaderField, streamIdentifier [4]byte, h2c *h2connection.H2Connection) error{
	//Encode response header with HPACK and store it in headerBuffer
	//EncodeHeader(responseHeader, hpackEncoder)

	err := h2c.EncodeHeader(responseHeader)
	if err != nil{
		return errors.New("SendHeader error (encoding): " + err.Error())
	}

	//Create response frame
	responseHeaderFrame := frame.NewHeaderFrame(h2c.HeaderBuffer.Len(), streamIdentifier, byte(4)) //flag 0x4 means entire responseHeader is in this frame
	headerFrameBuffer := responseHeaderFrame
	headerFrameBuffer = append(headerFrameBuffer, h2c.HeaderBuffer.Bytes()...)
	h2c.HeaderBuffer.Reset()

	//Send header frame
	//_, err = con.Write(headerFrameBuffer)
	fmt.Println("Sending header")
	err = h2c.Write(headerFrameBuffer)
	if err != nil{
		return errors.New("SendHeader error (writing): " + err.Error())
	}
	return nil
}

/**
	Encode response header with HPACK and store it in headerBuffer
*/
//func EncodeHeader(responseHeader []hpack.HeaderField, hpackEncoder *hpack.Encoder){
//	for _, field := range responseHeader {
//		err := hpackEncoder.WriteField(field)
//		if err != nil{
//			fmt.Println(err.Error())
//		}
//	}
//}

//Used to send HTTP error messages to client
//Supply with error (HTTP error code), requestHeader and the http2 connection
func SendError(error int, requestHeader *header.Request, h2c *h2connection.H2Connection) error{
	//Construct response header with error status
	responseHeader := []hpack.HeaderField{
		{Name: ":status", Value: strconv.Itoa(error)},
		{Name: "version", Value: "HTTP/2.0"},
	}

	//Encode response header with HPACK and store it in headerBuffer
	//EncodeHeader(responseHeader, hpackEncoder)
	err := h2c.EncodeHeader(responseHeader)
	if err != nil{
		return errors.New("SendHeader error (encoding): " + err.Error())
	}

	//Create response frame
	responseHeaderFrame := frame.NewHeaderFrame(h2c.HeaderBuffer.Len(), requestHeader.StreamIdentifier, byte(5)) //flag 0x4 + 0x1 means entire responseHeader is in this frame and this is last message from server on this stream
	headerFrameBuffer := responseHeaderFrame
	headerFrameBuffer = append(headerFrameBuffer, h2c.HeaderBuffer.Bytes()...)
	h2c.HeaderBuffer.Reset()

	fmt.Println("Headermessage: ", headerFrameBuffer)
	//Send header frame
	//_, err = con.Write(headerFrameBuffer)
	//if err != nil{
	//	return errors.New("Error writing error header: " + err.Error())
	//}
	err = h2c.Write(headerFrameBuffer)
	if err != nil{
		return errors.New("SendError error (writing): " + err.Error())
	}

	return nil
}

//Use this function to send servers settings.
//Currently just sends ACK message with no settings, effectively agreeing to client and default settings
func SendServerSettings(con net.Conn){
	fmt.Println("[Sending settings]")

	serverSettings := []byte{0, 0, 0, 4, 1, 0, 0, 0, 0} //start of settings frame
	//settings = append(settings, []byte{}...)
	_, err := con.Write(serverSettings)
	if err != nil{
		fmt.Println(err.Error())
	}
}