//Has a simple struct to store http2 connection settings
package settings

import (
	"encoding/binary"
	"strconv"
)

//struct to store http2 connection settings
type Settings struct{
	SETTINGS_HEADER_TABLE_SIZE uint32
	SETTINGS_ENABLE_PUSH uint32
	SETTINGS_MAX_CONCURRENT_STREAMS uint32
	SETTINGS_INITIAL_WINDOW_SIZE uint32
	SETTINGS_MAX_FRAME_SIZE uint32
	SETTINGS_MAX_HEADER_LIST_SIZE uint32
}

//Read settings from []byte to Settings object
func HandleSettings(payload []byte) Settings{
	var clientSettings Settings
	/**
		SETTINGS:
		SETTINGS_HEADER_TABLE_SIZE (0x1)
		SETTINGS_ENABLE_PUSH (0x2)
		SETTINGS_MAX_CONCURRENT_STREAMS (0x3)
		SETTINGS_INITIAL_WINDOW_SIZE (0x4)
		SETTINGS_MAX_FRAME_SIZE (0x5)
		SETTINGS_MAX_HEADER_LIST_SIZE (0x6)
 	*/
	clientSettings.SETTINGS_HEADER_TABLE_SIZE = 4096 //standard setting for header_table_size is 4096 octets
	clientSettings.SETTINGS_ENABLE_PUSH = 1 //standard setting for enable_push is 1 => yes
	clientSettings.SETTINGS_MAX_CONCURRENT_STREAMS = 100 //recommended minimum setting for max_concurrent_streams is 100
	clientSettings.SETTINGS_INITIAL_WINDOW_SIZE = 65535 //Initial window size 65535
	clientSettings.SETTINGS_MAX_FRAME_SIZE = 16384 //Initial max_frame_size 16384
	clientSettings.SETTINGS_MAX_HEADER_LIST_SIZE = 0 //Unknown initial setting. Set to 0 to ignore setting.

	for i:=0; i < len(payload); i+=6{ //paylodLength/6 = number of settings, since 1 setting is 2+4 bytes
		switch binary.BigEndian.Uint16(payload[i : i+2]) {
		case 1:
			clientSettings.SETTINGS_HEADER_TABLE_SIZE = binary.BigEndian.Uint32(payload[i+2:i+8])
		case 2:
			clientSettings.SETTINGS_ENABLE_PUSH = binary.BigEndian.Uint32(payload[i+2:i+8])
		case 3:
			clientSettings.SETTINGS_MAX_CONCURRENT_STREAMS = binary.BigEndian.Uint32(payload[i+2:i+8])
		case 4:
			clientSettings.SETTINGS_INITIAL_WINDOW_SIZE = binary.BigEndian.Uint32(payload[i+2:i+8])
		case 5:
			clientSettings.SETTINGS_MAX_FRAME_SIZE = binary.BigEndian.Uint32(payload[i+2:i+8])
		case 6:
			clientSettings.SETTINGS_MAX_HEADER_LIST_SIZE = binary.BigEndian.Uint32(payload[i+2:i+8])
		}
	}

	return clientSettings
}


//Used to print settings. Debugging purposes.
func ToString(clientSettings *Settings) string{
	return "SETTINGS_HEADER_TABLE_SIZE: \t\t" + strconv.Itoa(int(clientSettings.SETTINGS_HEADER_TABLE_SIZE)) + "\n" +
		"SETTINGS_ENABLE_PUSH: \t\t\t\t" + strconv.Itoa(int(clientSettings.SETTINGS_ENABLE_PUSH)) + "\n" +
		"SETTINGS_MAX_CONCURRENT_STREAMS: \t" + strconv.Itoa(int(clientSettings.SETTINGS_MAX_CONCURRENT_STREAMS)) + "\n" +
		"SETTINGS_INITIAL_WINDOW_SIZE: \t\t" +strconv.Itoa(int(clientSettings.SETTINGS_INITIAL_WINDOW_SIZE)) + "\n" +
		"SETTINGS_MAX_FRAME_SIZE: \t\t\t" + strconv.Itoa(int(clientSettings.SETTINGS_MAX_FRAME_SIZE)) + "\n" +
		"SETTINGS_MAX_HEADER_LIST_SIZE: \t\t" + strconv.Itoa(int(clientSettings.SETTINGS_MAX_HEADER_LIST_SIZE))
}
