//Used for handling received GOAWAY, sending GOAWAY and received RST
package http2error

//TODO: Change from con.write to h2c.write
//TODO: Add send RST function

import (
	"encoding/binary"
	"errors"
	"fmt"
	"gitlab.com/jhfylling/gohttp2/frame"
	"net"
)

//Constants meant to be exported to easily use error codes
const (
	NO_ERROR = 0
	PROTOCOL_ERROR = 1
	INTERNAL_ERROR = 2
	FLOW_CONTROL_ERROR = 3
	SETTINGS_TIMEOUT = 4
	STREAM_CLOSED = 5
	FRAME_SIZE_ERROR = 6
	REFUSED_STREAM = 7
	CANCEL = 8
	COMPRESSION_ERROR = 9
	CONNECT_ERROR = 10
	ENHANCE_YOUR_CALM = 11
	INADEQUATE_SECURITY = 12
	HTTP_1_1_REQUIRED = 13
)


//TODO: Respond with goaway and close connection
//Use this function to decode and print GOAWAY error messages
func HandleGoAway(payload []byte, con net.Conn) error{
	defer func(){
		conErr := con.Close()
		fmt.Println("Error closing connection: ", conErr)
	}()

	errorCode := binary.BigEndian.Uint32(payload[4:9])
	errorMessages := []string{"NO_ERROR", "PROTOCOL_ERROR", "INTERNAL_ERROR", "FLOW_CONTROL_ERROR", "SETTINGS_TIMEOUT", "STREAM_CLOSED", "FRAME_SIZE_ERROR", "REFUSED_STREAM", "CANCEL", "COMPRESSION_ERROR", "CONNECT_ERROR", "ENHANCE_YOUR_CALM", "INADEQUATE_SECURITY", "HTTP_1_1_REQUIRED"}
	fmt.Println("Goaway error: ", errorMessages[int(errorCode)], " Client: ", con.RemoteAddr())
	if len(payload) > 9{
		fmt.Println("Goaway additional debug data: ", string(payload[8:]))
	}

	goAwayErr := SendGoAway(con, NO_ERROR, [4]byte{payload[0],payload[1],payload[2],payload[3]})
	if goAwayErr != nil{
		return goAwayErr
	}
	return nil
}

//TODO: Use h2c.write
//Used to send GOAWAY messages
func SendGoAway(con net.Conn, errorCode byte, lastStream [4]byte) error{
	//Check that error code does not exceed the highest possible index, 13. (14 codes)
	if errorCode > byte(0xd){
		return errors.New("invalid GoAway error code")
	}

	goAwayPayload := []byte{lastStream[0], lastStream[1], lastStream[2], lastStream[3], 0,0,0, errorCode}

	goAwayFrame := []byte{0,0,8,7,0,lastStream[0], lastStream[1], lastStream[2], lastStream[3]}
	goAwayFrame = append(goAwayFrame, goAwayPayload...)

	_, err := con.Write(goAwayFrame)
	if err != nil{
		return errors.New("error sending GoAway: " + err.Error())
	}

	return nil
}

//TODO: Close stream referenced in RST message
//Use this function to decode RST messages and print error messages. (Could be NO_ERROR)
func HandleRST(httpFrame frame.Http2frame, con net.Conn){
	fmt.Println("[RST] for stream: ", httpFrame.StreamIdentifier, " Client: ", con.RemoteAddr())
	if len(httpFrame.Payload) > 0{
		errorCode := binary.BigEndian.Uint32(httpFrame.Payload)
		errorMessages := []string{"NO_ERROR", "PROTOCOL_ERROR", "INTERNAL_ERROR", "FLOW_CONTROL_ERROR", "SETTINGS_TIMEOUT", "STREAM_CLOSED", "FRAME_SIZE_ERROR", "REFUSED_STREAM", "CANCEL", "COMPRESSION_ERROR", "CONNECT_ERROR", "ENHANCE_YOUR_CALM", "INADEQUATE_SECURITY", "HTTP_1_1_REQUIRED"}
		fmt.Println("[RST error]: ", errorMessages[int(errorCode)])
	}
}