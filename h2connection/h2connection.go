//Used to keep track of one connection with decoder, encoder and tls connection objects.
//Used for reading and writing safely with asynchronous request handling.
package h2connection

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"gitlab.com/jhfylling/gohttp2/settings"
	"golang.org/x/net/http2/hpack"
	"io"
	"net"
	"sync"
)

//Contains necessary objects needed to keep using the same connection through multiple requests
//Also contains mutexes used to allow async io.
type H2Connection struct{
	Con            net.Conn
	HpackDecoder   *hpack.Decoder
	HpackEncoder   *hpack.Encoder
	HeaderBuffer   *bytes.Buffer
	ClientSettings settings.Settings

	Mux sync.Mutex
	WriteMux sync.Mutex
	ReadMux sync.Mutex
}

//Locks other threads out from decoding to ensure correct use of dynamic table
//Decodes header
//encodedHeader []byte from client with hpack encodig
//[]hpack.HeaderField unencoded headerfields
//returns error if decoding produces error
func (h2c *H2Connection)DecodeHeader(encodedHeader []byte) ([]hpack.HeaderField, error){
	h2c.Mux.Lock()

	decodedHeader, err := h2c.HpackDecoder.DecodeFull(encodedHeader)
	if err != nil{
		h2c.Mux.Unlock()
		return nil, err
	}

	h2c.Mux.Unlock()

	return decodedHeader, nil
}

//Locks other threads out from encoding to ensure correct use of dynamic table
//Encodes responseHeader []hpack.HeaderField and sends them to HeaderBuffer
//returns error if encoding produces error
func (h2c *H2Connection)EncodeHeader(responseHeader []hpack.HeaderField) error{
	h2c.Mux.Lock()
	h2c.HeaderBuffer.Reset()
	for _, field := range responseHeader {
		err := h2c.HpackEncoder.WriteField(field)
		if err != nil{
			h2c.HeaderBuffer.Reset()
			h2c.Mux.Unlock()
			return err
		}
	}

	h2c.Mux.Unlock()

	return nil
}


//Writes to tls connection, preventing frames from interfering
//msg []byte is the framed message to be sent
//returns error if con.Write() produces error
func (h2c *H2Connection)Write(msg []byte) error{
	fmt.Println("[WRITE] waiting for mutex")
	h2c.WriteMux.Lock()
	defer h2c.WriteMux.Unlock()

	_, err := h2c.Con.Write(msg)
	if err != nil{
		return err
	}

	return nil
}

//Reads from tls connection, preventing frames from interfering
//msg []byte is the framed message to be sent
//returns error if con.Write() produces error
func (h2c *H2Connection)Read() ([]byte, error){
	h2c.ReadMux.Lock()
	defer h2c.ReadMux.Unlock()

	inputBuffer := make([]byte, 16384)	//Create buffer of size READ_BUFFER_SIZE. TODO: Check optimal buffersize. Allow configuration
	requestLength, err := h2c.Con.Read(inputBuffer)	//Read message into buffer //TODO: Replace with threadsafe h2c.read
	if err != nil{
		//h2c.ReadMux.Unlock()
		return nil, errors.New("POST error reading: " + err.Error())
	}



	message := inputBuffer[:requestLength] //Convert buffer to array of correct length

	//h2c.ReadMux.Unlock()

	return message, nil
}


//Reads from the tls connection, making sure to only read one single http2 frame from the buffer at a time
func (h2c *H2Connection)ReadFrame()([]byte, error){
	h2c.ReadMux.Lock()
	defer h2c.ReadMux.Unlock()

	buff := make([]byte, 16384)
	c := bufio.NewReader(h2c.Con)

	//read first 3 bytes, which should be the frame length
	var size = []byte{0} //Set first byte to 0, because BigEndian.Uint32() needs 4 bytes
	for i:=0;i<3;i++{
		b, err := c.ReadByte()
		if err != nil {
			//TODO: Probably should send goaway
			//h2c.ReadMux.Unlock()
			return nil, err
		}
		size = append(size, b)
	}

	//Convert size in 4 bytes to int
	sizeInt := binary.BigEndian.Uint32(size)

	// read the full frame, or return an error
	_, err := io.ReadFull(c, buff[:sizeInt+6])
	if err != nil {
		//TODO: Probably should send goaway
		//h2c.ReadMux.Unlock()
		return nil, err
	}

	var result []byte
	result = append(result, size[1:]...)
	result = append(result, buff[:sizeInt+6]...)

	//h2c.ReadMux.Unlock()

	return result, nil
}