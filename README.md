**What is this?**  
**goh2** with current reponame **GoHTTP2** is a HTTP2 library written in Go for Go  
It's goal is to allow easily creating a webserver that utilizes H2. 
**Repo:** [gitlab.com/jhfylling/gohttp2](https://gitlab.com/jhfylling/gohttp2/)  
  
This is a school project and should currently not be used for production purposes.  
  
**Supported Clients:**  
I recommend using Google Chrome to test this, since different H2 clients behave in different ways.  
Library uses HTTPS and ALPN to establish HTTP2 connection  
  
**Current functionality**  
1.  GET requests with or without CONTINUATION frames
2.  POST requests with or without CONTINUATION frames and fragmented / not fragmented DATA frames
3.  Handles multiple clients concurrently
4.  Handles multiple requests asynchronously
5.  Answers PING
6.  Serves static files
7.  Easy middleware configuration
8.  Registers GOAWAY, and RST and logs interpreted error messages in console.
9.  HPACK encoding and decoding is done by golang's net/http2/hpack package
  
  
**TODO-list:**  
1.  Create header fragmentation functionality, using continuation frames. (For sending large headers)
2.  Reading from tls connection into buffer: Check optimal buffersize. Allow configuration of buffersize.
3.  Revize allowed folder structure and path requests for static files
4.  Make http2error package use h2connection with async-safe read and write functions
5.  Close stream when receiving RST
6.  Respond with GOAWAY and close connection when receiving GOAWAY
7.  Add send RST functionality  

in middleware package:
1. implement timeout for fragmented headers and data
2. BUG: Some get & post requests end in protocol_error for no apperent reason.
3. Handle priority changes.
4. Add PUSH_PROMISE functionality
5. Timeout connections correctly including GOAWAY



**Installation**  
**Repo is currently set to private. Installation with private repo:**  
  
**LINUX:**  
Since this is a private repository, use this command to allow terminal prompt in linux:  
env GIT_TERMINAL_PROMPT=1 go get -v gitlab.com/jhfylling/gohttp2  
  
**WINDOWS:**  
Prompts must be enabled to use go get. Alternatively manuall clone project and put package in $GOPATH/src/<pkg>  
  
**MAC:**  
Manually clone or use go get with prompts enabled.  
  
**If repo is no longer private:**  
Simply install package with go get gitlab.com/jhfylling/gohttp2  
  
**Usage**  
**Example:**  

```go
	app := goh2.NewApp("D:/Projects/go/src/goh2demo/devh2/build", "D:/Projects/go/src/goh2demo/server.key", "D:/Projects/go/src/goh2demo/server.crt", true)

	app.Get("/important/", func(req goh2.Request, res goh2.Response){
		fmt.Println("GET /important/")

		jsonTest := "{name: \"Johan\", age: 31, city: \"New York\", message: \"very important\"}"

		err := res.Send([]byte(jsonTest), "text/json")
		if err != nil{
			fmt.Println(err)
		}
	})

	app.Post("/importantpost", func(request goh2.Request, res goh2.Response){
		fmt.Println("POST /importantpost/")

		fmt.Println("POST data: ", string(request.Data))

		err := res.Send([]byte("Hello poster"), "text/plain")
		if err != nil{
			fmt.Println(err)
		}
	})


	goh2.Listen("localhost:443", &app) //address:port, app

```  
  
**Configure app**
```go
    app := goh2.NewApp("path to static files", "path to server.key", "path to server.crt", <serve static files true/false>)
```
  
**Add middleware**
```go
    app.Get("path", func(req goh2.Request, res goh2.Response){})
    app.Post("path", func(req goh2.Request, res goh2.Response){})
    app.Use("path", func(req goh2.Request, res goh2.Response){})
```
  
**Start listening**
```go
    goh2.Listen("address:port", &app) //address:port, app
```  
  
**External dependencies**  
golang.org/x/net/http2/hpack is part of the official Go Project, but is not included in the main Go tree.  
This is however automatically installed when using go get gitlab.com/jhfylling/gohttp2  
  
**Documentation**  
Source code documentation will be found at:  
[goh2](https://godoc.org/gitlab.com/jhfylling/gohttp2)  
sub packages:  
[app](https://godoc.org/gitlab.com/jhfylling/gohttp2/app)  
[frame](https://godoc.org/gitlab.com/jhfylling/gohttp2/frame)  
[gohttp2](https://godoc.org/gitlab.com/jhfylling/gohttp2/gohttp2)  
[h2connection](https://godoc.org/gitlab.com/jhfylling/gohttp2/h2connection)  
[header](https://godoc.org/gitlab.com/jhfylling/gohttp2/header)  
[http2error](https://godoc.org/gitlab.com/jhfylling/gohttp2/http2error)  
[middleware](https://godoc.org/gitlab.com/jhfylling/gohttp2/middleware)  
[response](https://godoc.org/gitlab.com/jhfylling/gohttp2/response)  
[settings](https://godoc.org/gitlab.com/jhfylling/gohttp2/settings)  
