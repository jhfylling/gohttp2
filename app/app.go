//Package contains the App struct with all endpoints and the public folder used to serve files
//Also has methods Get, Post, etc to register new endpoints
package app

import (
	"fmt"
	"gitlab.com/jhfylling/gohttp2/header"
	"gitlab.com/jhfylling/gohttp2/response"
)

//Struct which contains the HTTP command, URL path and a function to be executed by the endpoint
type Endpoint struct{
	Command  string //HTTP command
	Path     string //URL path
	Function func(req header.Request, res response.Response) //Endpoint function created by the web developers
}

//Struct that contains all Endpoints and the public folder used to serve files
type App struct{
	Endpoints []Endpoint
	PublicFolder string
	ServeStatic bool
	PrivateKeyPath string
	PublicKeyPath string
}

//Create new App. (Constructor)
func NewApp(publicFolder string, privateKeyPath string, publicKeyPath string, serveStatic bool) App{
	return App{PublicFolder: publicFolder, ServeStatic: serveStatic, PrivateKeyPath: privateKeyPath, PublicKeyPath: publicKeyPath}
}

//Register new GET endpoint
func (app *App) Get(path string, function func(req header.Request, res response.Response)){
	app.Endpoints = append(app.Endpoints, Endpoint{"GET", path, function})

	fmt.Println(app.Endpoints)
}

//Register new POST endpoint
func (app *App) Post(path string, function func(req header.Request, res response.Response)){
	app.Endpoints = append(app.Endpoints, Endpoint{"POST", path, function})
}

//Register new USE endpoint
func (app *App) Use(path string, function func(req header.Request, res response.Response)){
	app.Endpoints = append(app.Endpoints, Endpoint{"USE", path, function})
}

//Set the public folder that is used to serve files
func (app *App) SetPublicFolder(path string){
	app.PublicFolder = path
}

//Set ServeStatic to true, to make app serve static files in addition to endpoints
func (app *App) SetServeStatic(value bool){
	app.ServeStatic = value
}


