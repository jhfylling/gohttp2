//Used to interpret []byte as frames and creates frames on the Http2frame format
//and creates header and data frames
package frame

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
)

type Http2frame struct{
	FrameType        byte
	Payload          []byte
	StreamIdentifier [4]byte
	Flags            byte
	Preface          bool
}

func InterPretFrame(frame []byte) Http2frame{
	/**
		client preface: 0x505249202a20485454502f322e300d0a0d0a534d0d0a0d0a
		client preface: PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n
		followed by: 0000120400000000000001000100000003000003e800040060000000000408000000000000ef0001
	*/
	var http2Frame []byte
	preface := false
	if len(frame) >= 24{
		clientPreface := frame[:24]
		if hex.EncodeToString(clientPreface) == "505249202a20485454502f322e300d0a0d0a534d0d0a0d0a"{
			http2Frame = frame[24:]
			preface = true
		}else{
			http2Frame = frame
		}
	}else{
		http2Frame = frame
	}

	if len(http2Frame) >= 9{
		var payloadLength32 []byte
		payloadLength32 = append(payloadLength32, 0)
		payloadLength32 = append(payloadLength32, http2Frame...) //TODO: Check if this or line below is more correct
		//payloadLength32 = append(payloadLength32, http2Frame[0:3]...)
		payloadLength := binary.BigEndian.Uint32(payloadLength32)

		payloadType := http2Frame[3:4][0]
		payloadFlag := http2Frame[4:5][0]
		streamIdentifier := http2Frame[5:9]
		payload := http2Frame[9:9+payloadLength]
		return Http2frame{FrameType: payloadType, Payload: payload, StreamIdentifier: [4]byte{streamIdentifier[0], streamIdentifier[1], streamIdentifier[2], streamIdentifier[3]}, Flags: payloadFlag, Preface: preface}
	}
	return Http2frame{FrameType: 9, Payload: nil, StreamIdentifier: [4]byte{0,0,0,0}, Flags: 0, Preface: preface}
}

//TODO: Implement easy to use flag structure
/**
	Function creates first part of a header frame and returns it as []byte
*/
func NewHeaderFrame(payloadLength int, streamIdentifier [4]byte, flags byte)[]byte{
	var headerFrame []byte

	//Create 4 byte length and append last 3 bytes to headerFrame as Payload length
	payloadLengthBytes := make([]byte, 4)
	binary.BigEndian.PutUint32(payloadLengthBytes, uint32(payloadLength))
	headerFrame = append(headerFrame, payloadLengthBytes[1:]...)

	//Set type header
	headerFrame = append(headerFrame, byte(1)) //type 0x1 is header

	//Set Flags
	headerFrame = append(headerFrame, flags)

	//Set stream identifier
	headerFrame = append(headerFrame, streamIdentifier[:]...)

	/*//Set stream dependency
	headerFrame = append(headerFrame, []byte{0,0,0,1}...)

	//Set weight
	headerFrame = append(headerFrame, byte(255))*/

	fmt.Println("Header Frame: ", headerFrame)

	return headerFrame
}

/**
	Function creates a data frame and returns it as []byte
*/
func NewDataFrame(payload []byte, streamIdentifier [4]byte, flags byte)[]byte{
	var frame []byte

	//Create 4 byte length and append last 3 bytes to frame as Payload length
	payloadLengthBytes := make([]byte, 4)
	binary.BigEndian.PutUint32(payloadLengthBytes, uint32(len(payload)))
	frame = append(frame, payloadLengthBytes[1:]...)

	//Set type
	frame = append(frame, byte(0)) //type 0x0 is data

	//Set Flags
	frame = append(frame, flags)

	//Set stream identifier
	frame = append(frame, streamIdentifier[:]...)

	frame = append(frame, payload...)

	return frame
}
