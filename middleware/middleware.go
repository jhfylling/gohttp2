//Package is meant to be the entrypoint to the library with Listen() and handleConnection() functions
//Can use endpoints or serve static files
//Handles all connections and requests asynchronously
package middleware

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"gitlab.com/jhfylling/gohttp2/app"
	"gitlab.com/jhfylling/gohttp2/frame"
	"gitlab.com/jhfylling/gohttp2/gohttp2"
	"gitlab.com/jhfylling/gohttp2/h2connection"
	"gitlab.com/jhfylling/gohttp2/header"
	"gitlab.com/jhfylling/gohttp2/http2error"
	"gitlab.com/jhfylling/gohttp2/response"
	"gitlab.com/jhfylling/gohttp2/settings"
	"golang.org/x/net/http2/hpack"
	"net"
	"os"
	"strings"
)

//TODO: Use configuration file
const (
	CONN_TYPE = "tcp"
	CONN_TIMEOUT = 5000 //milliseconds
	READ_BUFFER_SIZE = 16384 //bytes
)

//TODO: Merge handleRequest and handlePost functions int one function
//TODO: Implement timeout
//Handle a single request on a connection.
func handleRequest(app *app.App, h2c *h2connection.H2Connection, h2Frame frame.Http2frame, res response.Response, dataChannel chan frame.Http2frame){

	//check if entire header is sent or if we need continuation frames
	if h2Frame.Flags & byte(4) != byte(4) {
		fmt.Println("[Need continuation frame]")
		var message frame.Http2frame
		payload := h2Frame.Payload
		for {
			fmt.Println("[Waiting for continuation frame]")
			message = <-dataChannel
			//check if message belongs to this stream
			if message.StreamIdentifier == h2Frame.StreamIdentifier {
				if message.Flags & byte(4) != byte(4) {
					fmt.Println("[Received one. Need more]")
					//Not last frame, wait for next frame
					payload = append(payload, message.Payload...)
				} else {
					//Last frame, entire header received. Continue.
					fmt.Println("[Received last continuation frame]")
					payload = append(payload, message.Payload...)
					break
				}
			}
		}
		h2Frame.Payload = payload
	}

	reqHeader := header.NewHttp2Header(h2Frame, h2c)

	fmt.Println("COMMAND: ", reqHeader.Command)
	if reqHeader.Command == "POST"{
		handlePost(app, h2c, res, reqHeader, dataChannel)
	}else{
		//Use middleware first, then serve static if no endpoint matches and ServeStatic option is enabled
		foundEndpoint := useEndpoints(app, &reqHeader, res)

		if !foundEndpoint && app.ServeStatic{
			gohttp2.ServeStaticRequest(h2Frame, app.PublicFolder, h2c)
		}else if !foundEndpoint && !app.ServeStatic{
			err := gohttp2.SendError(404, &reqHeader, h2c)
			if err != nil{
				fmt.Println(err.Error())
			}
		}
	}

}

//Waits for POST data and proceeds using POST endpoints, supplied with POST request header and payload
func handlePost(app *app.App, h2c *h2connection.H2Connection, res response.Response, reqHeader header.Request, dataChannel chan frame.Http2frame){
	//TODO: Implement timeout

	fmt.Println("Handling post")

	//wait for data frame. Check if frame belongs to this stream
	//Keep append message payload to reqHeader.Data until we receive end stream flag
	var message frame.Http2frame
	for{
		message = <- dataChannel
		if message.StreamIdentifier == reqHeader.StreamIdentifier{
			if message.Flags & byte(1) == byte(1){ //End stream flag set. Entire POST received
				reqHeader.Data = append(reqHeader.Data, message.Payload...)
				break
			}else{
				reqHeader.Data = append(reqHeader.Data, message.Payload...)
			}
		}
	}

	//reqHeader.Data = message.Payload //Set initial reqHeader.Data

	//Use endpoints and send 404 if no endpoints match
	if !usePostEndpoints(app, &reqHeader, res){
		err := gohttp2.SendError(404, &reqHeader, h2c)
		if err != nil{
			fmt.Println(err.Error())
		}
	}
}

//Finds POST endpoint matching the request and executes endpoint function
func usePostEndpoints(e *app.App, reqHeader *header.Request, res response.Response) bool{
	path := reqHeader.Path
	path = strings.Split(path, "?")[0]
	if strings.LastIndex(path, "/") != len(path)-1{
		path = path + "/"
	}
	command := &reqHeader.Command

	res.SetStreamIdentifier(reqHeader.StreamIdentifier)

	pathHasEndpoint := false
	//Loop through all endpoints in "added first, used first" order
	for _, endpoint := range e.Endpoints{
		//Check that endpoint command is equal to HTTP command
		if strings.ToUpper(endpoint.Command) == strings.ToUpper(*command){ //TODO: Replace string based command comparing with int comparison or byte operations to improve speed
			//Check if path contains endpoint path
			if strings.Contains(path, endpoint.Path){
				//execute endpoint function
				endpoint.Function(*reqHeader, res)

				pathHasEndpoint = true
			}
		}
	}
	return pathHasEndpoint
}

//Finds endpoint matching the request and executes endpoint function
func useEndpoints(e *app.App, reqHeader *header.Request, res response.Response) bool{
	path := reqHeader.Path
	path = strings.Split(path, "?")[0]

	//add trailing / if it's missing
	if strings.LastIndex(path, "/") != len(path)-1{
		path = path + "/"
	}
	command := &reqHeader.Command

	//Set response object streamIdentifier to request streamIdentifier
	res.SetStreamIdentifier(reqHeader.StreamIdentifier)

	pathHasEndpoint := false
	//Loop through all endpoints in "added first, used first" order
	for _, endpoint := range e.Endpoints{
		//Check that endpoint command is equal to HTTP command
		if strings.ToUpper(endpoint.Command) == strings.ToUpper(*command){
			//Check if path contains endpoint path
			if strings.Contains(path, endpoint.Path){
				//execute endpoint function
				endpoint.Function(*reqHeader, res)

				pathHasEndpoint = true
			}
		}
	}
	return pathHasEndpoint
}

//Keeps track of a single TLS connection
//Handles preface, keeps connection alive, handles requests asynchronously
func HandleConnection(con net.Conn, app app.App) {
	//Always close connection before returning
	defer func(){
		//This produces an error since the connection is all ready closed by timeout
		_ = con.Close()
		fmt.Println("[ CLOSED CONNECTION TO " + con.RemoteAddr().String() + " ]")
	}()

	var h2c h2connection.H2Connection
	h2c.HpackDecoder = hpack.NewDecoder(4096, func(field hpack.HeaderField){fmt.Println("test")})
	h2c.HeaderBuffer = new(bytes.Buffer)
	h2c.HpackEncoder = hpack.NewEncoder(h2c.HeaderBuffer)
	h2c.Con = con

	res := response.NewResponse(h2c.Con, h2c.HpackEncoder, h2c.HeaderBuffer, &h2c.ClientSettings, &h2c)

	//TODO: Handle preface before loop, with designated preface read function
	inputBuffer := make([]byte, READ_BUFFER_SIZE)	//Create buffer of size READ_BUFFER_SIZE. TODO: Check optimal buffersize. Allow configuration
	requestLength, err := con.Read(inputBuffer)	//Read message into buffer //TODO: Replace con.Read with h2c.ReadFrame()
	if err != nil{
		fmt.Println("read error: ", err.Error())
		return
	}


	message := inputBuffer[:requestLength] //Convert buffer to array of correct length

	fmt.Println("Got a message")


	fmt.Println("[MESSAGE]: ", message)


	http2Frame := frame.InterPretFrame(message)
	h2c.ClientSettings = settings.HandleSettings(http2Frame.Payload)
	fmt.Println(settings.ToString(&h2c.ClientSettings))

	//Configure Encoder according to ClientSettings
	h2c.HpackEncoder.SetMaxDynamicTableSizeLimit(h2c.ClientSettings.SETTINGS_HEADER_TABLE_SIZE)
	h2c.HpackEncoder.SetMaxDynamicTableSize(h2c.ClientSettings.SETTINGS_HEADER_TABLE_SIZE)

	gohttp2.SendServerSettings(con)

	dataChannel := make(chan frame.Http2frame)

	for {
		//inputBuffer := make([]byte, READ_BUFFER_SIZE)	//Create buffer of size READ_BUFFER_SIZE. TODO: Check optimal buffersize. Allow configuration
		//requestLength, err := con.Read(inputBuffer)	//Read message into buffer //TODO: Replace con.Read with h2c.ReadFrame()

		message, err := h2c.ReadFrame()
		if err != nil{
			fmt.Println("read error: ", err.Error())
			return
		}


		h2Frame := frame.InterPretFrame(message)

		//message := inputBuffer[:requestLength] //Convert buffer to array of correct length
		//Handle request in it's own go-routine to prevent blocking other requests


		//TODO: Find out why some of the get / post requests end in protocol_error for no good reason

		//go func() {

			fmt.Println("Got a message")


			fmt.Println("[MESSAGE]: ", message)


			switch frameType := h2Frame.FrameType; frameType {
			case byte(0):
				//data
				fmt.Println("[DATA]")
				dataChannel <- h2Frame
			case byte(1):
				//header
				//TODO: Handle fragmented header with continuation frames
				fmt.Println("[HEADER]")

				go handleRequest(&app, &h2c, h2Frame, res, dataChannel)

			case byte(2):
				//priority
				//TODO: Handle priority changes.
			case byte(3):
				//rst
				fmt.Println("[RST]")
				go http2error.HandleRST(h2Frame, con)
			case byte(4):
				//settings
				fmt.Println("[SETTINGS]")

				//Inerpret settings frame and store it in the settings object
				go func() {
					h2c.ClientSettings = settings.HandleSettings(h2Frame.Payload)
					fmt.Println(settings.ToString(&h2c.ClientSettings))

					//Configure Encoder according to ClientSettings
					h2c.HpackEncoder.SetMaxDynamicTableSizeLimit(h2c.ClientSettings.SETTINGS_HEADER_TABLE_SIZE)
					h2c.HpackEncoder.SetMaxDynamicTableSize(h2c.ClientSettings.SETTINGS_HEADER_TABLE_SIZE)

					gohttp2.SendServerSettings(con)
				}()
			case byte(5):
				//push_promise
				fmt.Println("[PUSH_PROMISE]")
				//TODO: Add PUSH_PROMISE functionality
			case byte(6):
				//ping
				fmt.Println("[PING]")
				go handlePing(h2Frame, h2c)

			case byte(7):
				fmt.Println("[GOAWAY]")
				go func(){
					err := http2error.HandleGoAway(h2Frame.Payload, con)
					if err != nil{
						fmt.Println(err.Error())
					}
				}()
			case byte(8):
				//window_update
				fmt.Println("[WINDOW_UPDATE]")
				//SendServerSettings(Con)
			case byte(9):
				//Continuation frame
				fmt.Println("[CONTINUATION]")
				dataChannel <- h2Frame
			default:
				fmt.Println("[NO FRAME TYPE - DEFAULT]")
				go gohttp2.SendServerSettings(con)
			}

		//}()

		//TODO: Send GoAway before closing due to timeout
		//Set connection timeout after CONN_TIMEOUT milliseconds
		//err = con.SetDeadline(time.Now().Add(CONN_TIMEOUT * time.Millisecond))
		//if err != nil{
		//	fmt.Println("Error with deadline: ", err.Error())
		//}
	}
}

//Responds to received ping
func handlePing(h2Frame frame.Http2frame, h2c h2connection.H2Connection){
	if h2Frame.Flags & byte(1) != byte(1){
		//ack flag not set. Send response
		responseFrame := []byte{0,0,8,6,1,0,0,0,0} //length 8, type 6, flag 0x1, streamIdentifier 0,0,0,0
		responseFrame = append(responseFrame, h2Frame.Payload...) //append same opaque data

		err := h2c.Write(responseFrame)
		if err != nil{
			fmt.Println("Error sending ping to ", h2c.Con.RemoteAddr(), " on stream: ", h2Frame.StreamIdentifier)
		}
	}
}

//Starts HTTP2 webserver on address:port with configured app
func Listen(address string, app *app.App){
	//TODO: Add configuration functionality
	//Load certificates.
	cer, err := tls.LoadX509KeyPair(app.PublicKeyPath, app.PrivateKeyPath)
	if err != nil{
		fmt.Println("Could not load certificates: ", err.Error())
		os.Exit(1)
	}

	//Configure tls socket
	config := &tls.Config{
		Certificates: []tls.Certificate{cer},
		GetConfigForClient: func(helloinfo *tls.ClientHelloInfo) (*tls.Config, error) {
			fmt.Println(helloinfo.SupportedProtos)
			conf := &tls.Config{
				Certificates: []tls.Certificate{cer},
				NextProtos: helloinfo.SupportedProtos, //If client sends supported protos h2 through alpn, we can establish a h2 connection. //TODO: add http fallback if h2 support is missing
			}
			return conf, nil
		},
	}

	//start listening
	conListener, err := tls.Listen(CONN_TYPE, address, config)
	if err != nil{
		fmt.Println("Could not open connection listener: ", err.Error())
		os.Exit(1)
	}

	//Always close listener
	defer func(){
		err := conListener.Close()
		if err != nil{
			fmt.Println("Error closing connection listener: ", err.Error())
			os.Exit(1)
		}
	}()

	fmt.Println("Listening on " + address)

	//Accept incoming connections and serve them in separate go-routines
	for {
		conn, err := conListener.Accept()
		if err != nil{
			fmt.Println("error: ", err.Error())
		}

		fmt.Println("[ New connection from: " + conn.RemoteAddr().String() + " ]")
		go HandleConnection(conn, *app)
	}
}


