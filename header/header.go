//Used to create request header on the Request format
package header

import (
	"fmt"
	"gitlab.com/jhfylling/gohttp2/frame"
	"gitlab.com/jhfylling/gohttp2/h2connection"
)

//Easy to use header object that contains all relevant information
//Including RequestMap in which any information can be added by middleware
type Request struct{
	StreamIdentifier [4]byte
	Path string
	Command string
	Data []byte
	RequestMap map[string] interface{}
}

//Create Request from Http2frame
func NewHttp2Header(headerFrame frame.Http2frame, h2c *h2connection.H2Connection) Request {
	var header Request

	//hpackDecoder := hpack.NewDecoder(4096, func(field hpack.HeaderField){fmt.Println("test")})
	//hpackDecoder.SetMaxDynamicTableSize(reqObject.settings[0])

	header.StreamIdentifier = headerFrame.StreamIdentifier

	/*
		FLAGS:
		END_STREAM 	0x1 Last frame from sender on this stream
		END_HEADERS 0x4 Entire header in this frame
		PADDED		0x8	Pad length present (1 byte)
		PRIORITY	0x20 E, stream dependency and weight present (5 bytes)
	*/
	//Check for relevant flags
	startOfHeader := 0
	if headerFrame.Flags & byte(0x20) == byte(0x20){ //E, stream dependency and weight specified in first 5 bytes
		startOfHeader += 5
	}
	if headerFrame.Flags & byte(0x8) == byte(0x8){ //Padding length specified in first byte
		startOfHeader += 1
	}
	if headerFrame.Flags & byte(0x4) != byte(0x4){
		fmt.Println("[WARNING]: Entire header not in this frame. Need continuation frame")
	}
	if headerFrame.Flags & byte(0x1) != byte(0x1){
		fmt.Println("[WARNING]: Sender can reuse stream")
	}

	//Decode header using hpack
	encodedHeader := headerFrame.Payload[startOfHeader:]
	decodedHeader, err := h2c.DecodeHeader(encodedHeader)
	//decodedHeader, err := hpackDecoder.DecodeFull(encodedHeader)
	if err != nil{
		fmt.Println("[Decoding error]: ", err.Error())
	}

	fmt.Println("[HEADER]: ", decodedHeader)

	//TODO: Suggest allowing all paths lower than current folder? Or configured folder perhaps, like www folder

	//Find requested path in header
	pathSpecified := false
	for _, field := range decodedHeader{
		if field.Name == ":path"{
			fmt.Println(field)
			pathSpecified = true
			if field.Value == "/"{
				header.Path = "/index.html"
			}else{
				header.Path = field.Value
			}
		}
		if field.Name == ":method"{
			header.Command = field.Value
		}
	}
	if !pathSpecified{
		header.Path = "/index.html"
	}

	return header
}
